<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonsController extends Controller
{
    public function __invoke()
    {
        $persons = [

            [
                'id' => 1,
                'name' => 'John',
                'age' => 20,
                'job' => 'driver',
            ],
            [
                'id' => 2,
                'name' => 'Jane',
                'age' => 234,
                'job' => 'driver',
            ],
            [
                'id' => 3,
                'name' => 'Joererrrrhn',
                'age' => 2320,
                'job' => 'driver',
            ],
            [
                'id' => 4,
                'name' => 'eterer',
                'age' => 24230,
                'job' => 'driver',
            ],

        ];

        return $persons;
    }
}
