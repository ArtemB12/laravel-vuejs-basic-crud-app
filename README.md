## Summary

This is a simple Laravel/Vue3/VueX project where you can execute basic CRUD concepts with SQL base.
<br>
Styles are managed with Bootstrap library.

## Launch

- check your server running (XAMPP etc.)
- run 'npm run dev' in a terminal
- run 'php artisan serve' in another terminal window
- see the project at you localhost URL
