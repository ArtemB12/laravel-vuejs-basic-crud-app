import { createStore} from "vuex";
import { PersonModule } from "./modules/person";

export default createStore({
    modules: {
        post: PersonModule
    }
})
